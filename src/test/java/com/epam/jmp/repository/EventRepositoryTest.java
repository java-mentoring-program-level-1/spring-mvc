package com.epam.jmp.repository;

import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.epam.jmp.model.Event;
import com.epam.jmp.model.impl.EventImpl;
import com.epam.jmp.repository.impl.EventRepositoryImpl;
import com.epam.jmp.storage.BookingStorage;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(value = "/config/daos-context.xml")
public class EventRepositoryTest {
	
	@Autowired
	SimpleDateFormat formatter;
	@Autowired
	EventRepository eventRepository;
	
	@Test
	public void shouldReturnNewEventWhenCreateEvent() {
		Event event = new EventImpl();
		event.setDate(new Date());
		event.setTitle("good event");
		Event result = eventRepository.createEvent(event);
		assertTrue(result.getId() > 0);
		assertEquals(event.getDate(), result.getDate());
		assertEquals(event.getTitle(), result.getTitle());
	}
	
	@Test
	public void shouldThrowExceptionWhenCreateEventWithEmptyFields() {
		Event event = new EventImpl();
		assertThrows(IllegalArgumentException.class, () -> {
			eventRepository.createEvent(event);
		});
	}
	
	@Test 
	public void shouldThrowExceptionWhenCreateEventWithInvalidDate() throws ParseException {
		Event event = new EventImpl();
		event.setDate(formatter.parse("10-12-2021 14:00:00"));
		event.setTitle("old event");
		assertThrows(IllegalArgumentException.class, () -> {
			eventRepository.createEvent(event);
		});
	}
	
	@Test
	public void shouldReturnEventWhenGetEventById() {
		long eventId = 1L;
		Optional<Event> eventOptional = eventRepository.getEventById(eventId);
		assertTrue(eventOptional.isPresent());
		assertEquals(eventId, eventOptional.get().getId());
	}
	
	@Test
	public void shouldReturnEmptyEventWhenGetEventById() {
		long eventId = 100L;
		Optional<Event> eventOptional = eventRepository.getEventById(eventId);
		assertFalse(eventOptional.isPresent());
	}
	
	@Test
	public void shouldReturnUpdatedEventWhenUpdateEvent() throws ParseException {
		Event event = new EventImpl();
		event.setId(1L);
		Date date = formatter.parse("12-11-2021 13:37:15");
		event.setDate(date);
		event.setTitle("today's boring event");
		
		Event updatedEvent = eventRepository.updateEvent(event);
		assertNotNull(updatedEvent);
		assertEquals(event.getId(), updatedEvent.getId());
		assertEquals(date, updatedEvent.getDate());
		assertEquals(event.getTitle(), updatedEvent.getTitle());
	}
	
	@Test
	public void shouldThrowExceptionWhenUpdateEventWithNullEvent() {
		Event event = null;
		assertThrows(IllegalArgumentException.class, () -> {
			eventRepository.updateEvent(event);
		});
	}
	
	@Test
	public void shouldThrowExceptionWhenUpdateEventWithNullDate() {
		Event event = new EventImpl();
		event.setId(1L);
		event.setTitle("tech world");
		
		assertThrows(IllegalArgumentException.class, () -> {
			eventRepository.updateEvent(event);
		});
	}
	
	@Test
	public void shouldThrowExceptionWhenUpdateEventWithInvalidTitle() {
		Event event = new EventImpl();
		event.setId(1L);
		event.setTitle("");
		event.setDate(new Date());
		
		assertThrows(IllegalArgumentException.class, () -> {
			eventRepository.updateEvent(event);
		});
	}
	
	@Test
	public void shouldThrowExceptionWhenUpdateEventWithInvalidId() {
		Event event = new EventImpl();
		event.setId(0L);
		event.setTitle("invalid id event");
		event.setDate(new Date());
		
		assertThrows(IllegalArgumentException.class, () -> {
			eventRepository.updateEvent(event);
		});
	}
	
	@Test
	public void shouldReturnTrueWhenDeleteEventById() {
		long eventId = 1L;
		boolean result = eventRepository.deleteEventById(eventId);
		assertTrue(result);
	}
	
	@Test
	public void shouldReturnFalseWhenDeleteEventById() {
		long eventId = 0L;
		boolean result = eventRepository.deleteEventById(eventId);
		assertFalse(result);
	}
	
	@Test
	public void shouldReturnEventsWhenGetEventsByTitle() {
		String title = "System";
		int pageSize = 5;
		int pageNumber = 1;
		List<Event> events = eventRepository.getEventsByTitle(title, pageSize, pageNumber);
		assertTrue(events.size() == 2 && events.size() <= pageSize);
	}
	
	@Test
	public void shouldReturnEmptyEventListWhenGetEventsByTitle() {
		String title = "event";
		int pageSize = 5;
		int pageNumber = 2;
		List<Event> events = eventRepository.getEventsByTitle(title, pageSize, pageNumber);
		assertTrue(events.size() == 0 && events.size() <= pageSize);
	}
	
	@Test
	public void shouldThrowExceptionWhenGetEventsByTitleWithInvalidPageNumber() {
		assertThrows(IllegalArgumentException.class, () -> {
			eventRepository.getEventsByTitle("event", 5, 0);
		});
	}
	
	@Test
	public void shouldGetEventsWhenGetEventsForDay() throws ParseException {
		formatter.applyPattern("dd-MM-yyyy");
		Date day = formatter.parse("20-12-2021");
		int pageSize = 5;
		int pageNumber = 1;
		List<Event> events = eventRepository.getEventsForDay(day, pageSize, pageNumber);
		assertTrue(events.size() == 2);
	}
	
	@Test
	public void shouldGetEmptyEventListWhenGetEventsForDay() throws ParseException {
		formatter.applyPattern("dd-MM-yyyy");
		Date day = formatter.parse("22-01-2017");
		int pageSize = 5;
		int pageNumber = 1;
		List<Event> events = eventRepository.getEventsForDay(day, pageSize, pageNumber);
		assertTrue(events.size() == 0);
	}
	
	@Test
	public void shouldThrowExceptionWhenGetEventsForDayWithNullDay() throws ParseException {
		formatter.applyPattern("dd-MM-yyyy");
		Date day = null;
		int pageSize = 5;
		int pageNumber = 1;
		assertThrows(IllegalArgumentException.class, () -> {
			eventRepository.getEventsForDay(day, pageSize, pageNumber);	
		});
		
	} 
	
	@Test 
	public void shouldReturnTrueWhenIsPresent() {
		boolean result = eventRepository.isPresent(2L);
		
		assertTrue(result);
	}
	
	@Test 
	public void shouldReturnFalseWhenIsPresent() {
		boolean result = eventRepository.isPresent(1L);
		
		assertFalse(result);
	}
}
