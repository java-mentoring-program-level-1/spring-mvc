package com.epam.jmp.controller;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import javax.servlet.ServletContext;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockServletContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.ContextHierarchy;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.junit.jupiter.web.SpringJUnitWebConfig;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.ResultHandler;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

@ExtendWith(SpringExtension.class)
@SpringJUnitWebConfig
@ContextHierarchy({
  @ContextConfiguration(locations = "classpath:config/services-context.xml"),
  @ContextConfiguration(locations = "classpath:config/application-context.xml")
})
public class EventControllerTest {
  private static final String page = "1";
  private static final String size = "5";
  private static final String VIEW = "event.html";
  private static final String ERROR_VIEW = "error.html";

  private MockMvc mockMvc;

  @BeforeEach
  void setup(WebApplicationContext wac) {
    this.mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
  }

  @Test
  public void getEvents_title_shouldReturnOk() throws Exception {
    // Given
    final String title = "System";

    // When
    final ResultActions result = mockMvc.perform(get("/events")
      .param("title", title)
      .param("page", page)
      .param("size", size));

    // Then
    result.andExpect(status().isOk());
    result.andExpect(model().attributeExists("events"));
    result.andExpect(view().name(VIEW));
  }

  @Test
  public void getEvents_notExistingTitle_shouldReturnNotFound() throws Exception {
    // Given
    final String title = "cup Of Tea";

    // When
    final ResultActions result = mockMvc.perform(get("/events")
      .param("title", title)
      .param("page", page)
      .param("size", size));

    // Then
    result.andDo(print());
    result.andExpect(status().isNotFound());
    result.andExpect(view().name(ERROR_VIEW));
  }

  @Test
  public void getEvents_date_shouldReturnOk() throws Exception {
    // Given
    final String date = "2021-12-20";

    // When
    final ResultActions result = mockMvc.perform(get("/events")
      .param("date", date)
      .param("page", page)
      .param("size", size));

    // Then
    result.andExpect(status().isOk());
    result.andExpect(model().attributeExists("events"));
    result.andExpect(view().name(VIEW));
  }

  @Test
  public void getEvents_notExistingDate_shouldReturnNotFound() throws Exception {
    // Given
    final String date = "1999-12-20";

    // When
    final ResultActions result = mockMvc.perform(get("/events")
      .param("date", date)
      .param("page", page)
      .param("size", size));

    // Then
    result.andExpect(status().isNotFound());
    result.andExpect(view().name(ERROR_VIEW));
  }

  @Test
  public void getEvent_id_shouldReturnOk() throws Exception {
    // Given
    final String id = "1";

    // When
    ResultActions result = mockMvc.perform(get("/events")
      .param("id", id));

    // Then
    result.andExpect(status().isOk());
    result.andExpect(model().attributeExists("events"));
    result.andExpect(view().name(VIEW));
  }

  @Test
  public void getEvent_notExistingId_shouldReturnNotFound() throws Exception {
    // Given
    final String id = "156348";

    // When
    final ResultActions result = mockMvc.perform(get("/events")
      .param("id", id));

    // Then
    result.andExpect(status().isNotFound());
    result.andExpect(view().name(ERROR_VIEW));
  }

  @Test
  public void deleteEvent_id_shouldReturnOk() throws Exception {
    // Given
    final String id = "2";

    // When
    final ResultActions result = mockMvc.perform(get("/events")
      .param("_method", "delete")
      .param("id", id));

    // Then
    result.andExpect(status().isOk());
    result.andExpect(model().attribute("message", "Event with id = " + id + " is deleted successfully"));
    result.andExpect(view().name(VIEW));
  }

  @Test
  public void deleteEvent_notExistingId_shouldReturnOk() throws Exception {
    // Given
    final String id = "16345";

    // When
    final ResultActions result = mockMvc.perform(get("/events")
      .param("_method", "delete")
      .param("id", id));

    // Then
    result.andExpect(status().isOk());
    result.andExpect(model().attribute("message", "Event with id = " + id + " is not deleted"));
    result.andExpect(view().name(VIEW));
  }

  @Test
  public void insertEvent_event_shouldReturnOkAndEvent() throws Exception {
    // Given
    final String title = "Good Mood Event";
    final String date = "2022-12-20T06:43";

    // When
    final ResultActions result = mockMvc.perform(post("/events")
      .param("title", title)
      .param("date", date));

    // Then
    result.andExpect(status().isOk());
    result.andExpect(model().attributeExists("events"));
    result.andExpect(view().name(VIEW));
  }
}


