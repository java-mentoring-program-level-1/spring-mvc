package com.epam.jmp.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.epam.jmp.model.User;
import com.epam.jmp.model.impl.UserImpl;
import com.epam.jmp.repository.UserRepository;
import com.epam.jmp.service.impl.UserServiceImpl;

@ExtendWith(MockitoExtension.class)
public class UserServiceTest {
	UserServiceImpl userService;
	@Mock
	UserRepository userRepository;
	
	@BeforeEach
	public void init() {
		userService = new UserServiceImpl();
		userService.setUserRepository(userRepository);
	}
	
	@Test
	public void shouldCreateNewUserWhenCreateUser() {
		User user = new UserImpl();
		user.setId(1L);
		user.setEmail("newUser@email.com");
		user.setName("new user");
		
		when(userRepository.createUser(any())).thenReturn(user);
		User inputUser = new UserImpl();
		inputUser.setEmail("newUser@email.com");
		user.setName("new user");
		userService.createUser(inputUser);
		
		verify(userRepository, times(1)).createUser(any());
	}
	
	@Test
	public void shouldThrowExceptionWhenCreateUser() {
		User user = new UserImpl();
		user.setEmail("");
		user.setName("new user");
		
		when(userRepository.createUser(user)).thenThrow(IllegalArgumentException.class);
		assertThrows(IllegalArgumentException.class, () -> {
			userService.createUser(user);
		});
		
		verify(userRepository, times(1)).createUser(any());
	}
	
	@Test
	public void shouldGetUserWhenGetUserById() {
		long userId = 1L;
		User user = new UserImpl();
		user.setId(1L);
		user.setName("user one");
		user.setEmail("userOne@email.com");
		
		when(userRepository.getUserById(userId)).thenReturn(Optional.of(user));
		
		Optional<User> userOptional = userService.getUserById(userId);
		assertTrue(userOptional.isPresent());
		verify(userRepository, times(1)).getUserById(userId);
	}
	
	@Test
	public void shouldGetEmptyUserWhenGetUserById() {
		long userId = 1L;
		when(userRepository.getUserById(userId)).thenReturn(Optional.empty());
		
		Optional<User> userOptional = userService.getUserById(userId);
		assertFalse(userOptional.isPresent());
		verify(userRepository, times(1)).getUserById(userId);
	}
	
	@Test
	public void shouldGetUserWhenGetUserByEmail() {
		String email = "newUser@email.com";
		User user = new UserImpl();
		user.setId(1L);
		user.setEmail("newUser@email.com");
		user.setName("new user");
		when(userRepository.getUserByEmail(email)).thenReturn(Optional.of(user));
		
		Optional<User> userOptional = userService.getUserByEmail(email);
		assertTrue(userOptional.isPresent());
		
		verify(userRepository, times(1)).getUserByEmail(email);
	}
	
	@Test
	public void shouldReturnEmptyWhenGetUserByEmail() {
		String email = null;
		when(userRepository.getUserByEmail(email)).thenReturn(Optional.empty());
		
		Optional<User> userOptional = userService.getUserByEmail(email);
		assertFalse(userOptional.isPresent());
		
		verify(userRepository, times(1)).getUserByEmail(email);
	}
	
	@Test
	public void shouldReturnUpdatedUserWhenUpdateUser() {
		User user = new UserImpl();
		user.setId(1L);
		user.setEmail("newUser@email.com");
		user.setName("new User");
		
		User updatedUser = new UserImpl();
		user.setId(1L);
		user.setEmail("updateUser@email.com");
		user.setName("new user");
		
		when(userRepository.updateUser(user)).thenReturn(updatedUser);
		
		User result = userService.updateUser(user);
		
		assertEquals(updatedUser.getId(), result.getId());
		assertEquals(updatedUser.getEmail(), result.getEmail());
		assertEquals(updatedUser.getName(), result.getName());
	}
	
	@Test
	public void shouldReturnExceptionWhenUpdateUserWithInvalidId() {
		User user = new UserImpl();
		user.setId(0);
		user.setEmail("newUser@email.com");
		user.setName("new User");
		when(userRepository.updateUser(user)).thenThrow(IllegalArgumentException.class);
		
		assertThrows(IllegalArgumentException.class, ()-> {
			userService.updateUser(user);
		});
	}
	
	@Test
	public void shouldReturnExceptionWhenUpdateUserWithInvalidEmail() {
		User user = new UserImpl();
		user.setId(1L);
		user.setEmail("  ");
		user.setName("new User");
		when(userRepository.updateUser(user)).thenThrow(IllegalArgumentException.class);
		
		assertThrows(IllegalArgumentException.class, ()-> {
			userService.updateUser(user);
		});
	}
	
	@Test
	public void shouldReturnExceptionWhenUpdateUserWithInvalidUser() {
		User user = null;
		when(userRepository.updateUser(user)).thenThrow(IllegalArgumentException.class);
		
		assertThrows(IllegalArgumentException.class, ()-> {
			userService.updateUser(user);
		});
	}
	
	@Test
	public void shouldReturnTrueWhenDeleteUser() {
		
		when(userRepository.deleteUserById(1L)).thenReturn(true);
		boolean result = userService.deleteUserById(1L);
		assertTrue(result);
	}
	
	@Test
	public void shouldReturnFalseWhenDeleteUser() {
		
		when(userRepository.deleteUserById(1L)).thenReturn(false);
		boolean result = userService.deleteUserById(1L);
		assertFalse(result);
	}
	
	@Test
	public void shouldReturnUsersWhenGetUsersByName() {
		User userOne = new UserImpl();
		userOne.setId(1L);
		userOne.setEmail("userOne@email.com");
		userOne.setName("user one");
		
		User userTwo = new UserImpl();
		userTwo.setId(2L);
		userTwo.setEmail("userTwo@email.com");
		userTwo.setName("user two");
		
		String name = "user";
		int pageSize = 5;
		int pageNumber = 1;
		when(userRepository.getUsersByName(name, pageSize, pageNumber)).thenReturn(List.of(userOne, userTwo));
		
		List<User> users = userService.getUsersByName(name, pageSize, pageNumber);
		
		assertTrue(users.size() == 2);
	}
	
	@Test 
	public void shouldReturnEmptyListWhenGetUsersByName() {
		String name = "new user";
		when(userRepository.getUsersByName("new user", 5, 2)).thenReturn(Collections.emptyList());
		List<User> users = userService.getUsersByName(name, 5, 2);
		assertTrue(users.size() == 0);
	}
	
	@Test
	public void shouldThrowExceptionWhenGetUsersByName() {
		String name = "name user";
		when(userRepository.getUsersByName(name, 5, 0)).thenThrow(IllegalArgumentException.class);
		assertThrows(IllegalArgumentException.class, () -> {
			userService.getUsersByName(name, 5, 0);
		});
	}
}
