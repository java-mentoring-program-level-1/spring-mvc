package com.epam.jmp.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.epam.jmp.model.Event;
import com.epam.jmp.model.exception.EntityNotFoundException;
import com.epam.jmp.model.impl.EventImpl;
import com.epam.jmp.repository.EventRepository;
import com.epam.jmp.service.impl.EventServiceImpl;

@ExtendWith(MockitoExtension.class)
public class EventServiceTest {
	
	@Mock
	EventRepository repository;
	EventServiceImpl service;
	SimpleDateFormat formatter;
	
	@BeforeEach
	public void init() {
		service = new EventServiceImpl();
		service.setRepository(repository);
		formatter = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");
	}
	
	@Test
	public void shouldReturnNewEventWhenCreateEvent() throws ParseException {
		
		Event newEvent = new EventImpl();
		Date eventDate = formatter.parse("22-01-2022 01:11:12");
		String eventTitle = "Black event";
		newEvent.setId(1L);
		newEvent.setDate(eventDate);
		newEvent.setTitle(eventTitle);
		
		when(repository.createEvent(any())).thenReturn(newEvent);
		
		Event event = new EventImpl();
		event.setDate(eventDate);
		event.setTitle(eventTitle);
		
		Event result = service.createEvent(event);
		assertEvent(newEvent, result);
	}
	
	@Test
	public void shouldThrowExceptionWhenCreateEventWithEmptyFields() {
		Event event = new EventImpl();
		when(repository.createEvent(event)).thenThrow(IllegalArgumentException.class);
		assertThrows(IllegalArgumentException.class, () -> {
			service.createEvent(event);
		});
	}
	
	@Test 
	public void shouldThrowExceptionWhenCreateEventWithInvalidDate() throws ParseException {
		Event event = new EventImpl();
		event.setDate(formatter.parse("01-12-2021 14:00:00")); // date is before today's date
		event.setTitle("old event");
		when(repository.createEvent(event)).thenThrow(IllegalArgumentException.class);
		assertThrows(IllegalArgumentException.class, () -> {
			service.createEvent(event);
		});
	}
	
	@Test
	public void shouldReturnEventWhenGetEventById() throws ParseException {
		Event event = new EventImpl();
		long eventId = 1L;
		event.setId(eventId);
		event.setDate(formatter.parse("01-12-2021 12:21:22"));
		event.setTitle("Fanta event");
		
		when(repository.getEventById(eventId)).thenReturn(Optional.of(event));
		
		Optional<Event> result = service.getEventById(eventId);
		assertTrue(result.isPresent());
		assertEvent(event, result.get());
	}
	
	@Test
	public void shouldReturnEmptyEventWhenGetEventById() {
		when(repository.getEventById(any(Long.class))).thenReturn(Optional.empty());
		Optional<Event> result = service.getEventById(1L);
		assertFalse(result.isPresent());
	}
	
	@Test
	public void shouldReturnUpdatedEventWhenUpdateEvent() throws ParseException {
		Event updatedEvent = new EventImpl();
		updatedEvent.setId(2L);
		updatedEvent.setDate(formatter.parse("03-04-2022 12:00:00"));
		updatedEvent.setTitle("Updated event");
		
		when(repository.updateEvent(any(Event.class))).thenReturn(updatedEvent);
		
		Event event = new EventImpl();
		event.setId(2L);
		event.setDate(new Date());
		event.setTitle("Black event");
		Event result = service.updateEvent(event);
		
		assertEvent(updatedEvent, result);
	}
	
	@Test
	public void shouldReturnTrueWhenDeleteEvent() {
		long eventId = 1L;
		when(repository.deleteEventById(eventId)).thenReturn(true);
		
		boolean result = service.deleteEventById(eventId);
		assertTrue(result);
	}
	
	@Test
	public void shouldReturnFalseWhenDeleteEvent() {
		long eventId = 2L;
		when(repository.deleteEventById(eventId)).thenReturn(false);
		
		boolean result = service.deleteEventById(eventId);
		assertFalse(result);
	}
	
	@Test
	public void shouldReturnEventsWhenGetEventsByTitle() {
		Event event1 = new EventImpl();
		event1.setId(1L);
		event1.setDate(new Date());
		event1.setTitle("evvent one");
		
		Event event2 = new EventImpl();
		event2.setId(2L);
		event2.setDate(new Date());
		event2.setTitle("evvent two");
		
		when(repository.getEventsByTitle("evvent", 5, 1)).thenReturn(List.of(event1, event2));
		List<Event> events = service.getEventsByTitle("evvent", 5, 1);
		assertEvent(event1, events.get(0));
		assertEvent(event2, events.get(1));
	}
	
	@Test
	public void shouldReturnEmptyListsWhenGetEventsByTitle() {
		String title = "no event";
		int size = 5;
		int page = 1;

		when(repository.getEventsByTitle(title, size, page)).thenReturn(Collections.emptyList());

		assertThrows(EntityNotFoundException.class, () -> {
			service.getEventsByTitle(title, size, page);
		});
	}
	
	@Test
	public void shouldReturnEventsWhenGetEventsForDay() throws ParseException {
		Event event1 = new EventImpl();
		event1.setId(1L);
		event1.setDate(formatter.parse("20-01-2012 15:00:00"));
		event1.setTitle("evvent one");
		
		Event event2 = new EventImpl();
		event2.setId(2L);
		event2.setDate(formatter.parse("20-01-2012 19:00:00"));
		event2.setTitle("evvent two");

		formatter.applyPattern("dd-MM-yyyy");
		Date eventDay = formatter.parse("20-01-2012");
		when(repository.getEventsForDay(eventDay, 5, 1)).thenReturn(List.of(event1, event2));
		List<Event> events = service.getEventsForDay(eventDay, 5, 1);
		assertEvent(event1, events.get(0));
		assertEvent(event2, events.get(1));
	}
	
	@Test
	public void shouldThrowExceptionWhenGetEventsForNotExistingDay() throws ParseException {
		formatter.applyPattern("dd-MM-yyyy");
		Date eventDay = formatter.parse("01-01-1999");

		when(repository.getEventsForDay(eventDay, 5, 1)).thenReturn(Collections.emptyList());

		assertThrows(EntityNotFoundException.class, ()-> {
			service.getEventsForDay(eventDay, 5, 1);
		});
	}
	
	private void assertEvent(Event actualEvent, Event expectedEvent) {
		assertEquals(actualEvent.getId(), expectedEvent.getId());
		assertTrue(actualEvent.getDate().toString().equals(expectedEvent.getDate().toString()));
		assertEquals(actualEvent.getTitle(), expectedEvent.getTitle());
	}
}
