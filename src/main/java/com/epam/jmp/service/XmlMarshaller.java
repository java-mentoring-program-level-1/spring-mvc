package com.epam.jmp.service;

public interface XmlMarshaller<T> {
  T unmarshal();
}
