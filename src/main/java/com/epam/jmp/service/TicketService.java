package com.epam.jmp.service;

import java.io.ByteArrayInputStream;
import java.util.List;

import com.epam.jmp.model.Event;
import com.epam.jmp.model.Ticket;
import com.epam.jmp.model.Ticket.Category;
import com.epam.jmp.model.User;

public interface TicketService {
	List<Ticket> getBookedTicket(User user, int pageSize, int pageNumber);
	List<Ticket> getBookedTicket(Event event, int pageSize, int pageNumber);
	Ticket bookTicket(long userId, long eventId, int place, Category category);
	boolean cancelTicket(long ticketId);
	ByteArrayInputStream generatePDFReport(List<Ticket> tickets);
	void preloadTickets();
}
