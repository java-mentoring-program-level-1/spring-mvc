package com.epam.jmp.service.impl;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.epam.jmp.model.Ticket;
import com.epam.jmp.service.PDFGenerator;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

public class TicketPDFGenerator implements PDFGenerator<Ticket> {
  private static Logger log = LoggerFactory.getLogger(TicketPDFGenerator.class);
  private static final int COLUMN_NUMBER = 5;
  private static final float WIDTH_PERCENTAGE = 60f;
  private static final String COLUMN_ID = "id";
  private static final String COLUMN_USER_ID = "user id";
  private static final String COLUMN_EVENT_ID = "event id";
  private static final String COLUMN_CATEGORY = "category";
  private static final String COLUMN_PLACE = "place";

  @Override
  public ByteArrayInputStream generate(List<Ticket> tickets) {
    Document document = new Document();
    ByteArrayOutputStream out = new ByteArrayOutputStream();
    try {

      PdfPTable table = initTable();

      populateTable(table, tickets);

      PdfWriter.getInstance(document, out);
      document.open();
      document.add(table);
      document.close();
    } catch (DocumentException ex) {
      log.error("Error occurred while generating pdf file: {0}", ex);
    }
    return new ByteArrayInputStream(out.toByteArray());
  }

  private void populateTable(PdfPTable table, List<Ticket> tickets) {
    for(Ticket ticket: tickets) {
      PdfPCell cell;

      cell = new PdfPCell(new Phrase(String.valueOf(ticket.getId())));
      cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
      cell.setHorizontalAlignment(Element.ALIGN_CENTER);
      table.addCell(cell);

      cell = new PdfPCell(new Phrase(String.valueOf(ticket.getUserId())));
      cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
      cell.setHorizontalAlignment(Element.ALIGN_CENTER);
      table.addCell(cell);

      cell = new PdfPCell(new Phrase(String.valueOf(ticket.getEventId())));
      cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
      cell.setHorizontalAlignment(Element.ALIGN_CENTER);
      table.addCell(cell);

      cell = new PdfPCell(new Phrase(ticket.getCategory().toString()));
      cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
      cell.setHorizontalAlignment(Element.ALIGN_CENTER);
      table.addCell(cell);

      cell = new PdfPCell(new Phrase(String.valueOf(ticket.getPlace())));
      cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
      cell.setHorizontalAlignment(Element.ALIGN_CENTER);
      table.addCell(cell);
    }
  }

  private PdfPTable initTable() throws DocumentException {
    PdfPTable table = new PdfPTable(COLUMN_NUMBER);
    table.setWidthPercentage(WIDTH_PERCENTAGE);
    table.setWidths(new int[] {1, 3, 3, 3, 3});

    Font headFont = FontFactory.getFont(FontFactory.HELVETICA_BOLD);

    PdfPCell hcell;
    hcell = new PdfPCell(new Phrase(COLUMN_ID, headFont));
    hcell.setHorizontalAlignment(Element.ALIGN_CENTER);
    table.addCell(hcell);

    hcell = new PdfPCell(new Phrase(COLUMN_USER_ID, headFont));
    hcell.setHorizontalAlignment(Element.ALIGN_CENTER);
    table.addCell(hcell);

    hcell = new PdfPCell(new Phrase(COLUMN_EVENT_ID, headFont));
    hcell.setHorizontalAlignment(Element.ALIGN_CENTER);
    table.addCell(hcell);

    hcell = new PdfPCell(new Phrase(COLUMN_CATEGORY, headFont));
    hcell.setHorizontalAlignment(Element.ALIGN_CENTER);
    table.addCell(hcell);

    hcell = new PdfPCell(new Phrase(COLUMN_PLACE, headFont));
    hcell.setHorizontalAlignment(Element.ALIGN_CENTER);
    table.addCell(hcell);
    return table;
  }
}
