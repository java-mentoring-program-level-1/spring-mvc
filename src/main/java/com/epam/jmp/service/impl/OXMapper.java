package com.epam.jmp.service.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;

import javax.xml.transform.stream.StreamSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ClassPathResource;
import org.springframework.oxm.Marshaller;
import org.springframework.oxm.Unmarshaller;

import com.epam.jmp.model.Ticket;
import com.epam.jmp.model.impl.TicketContainer;


public class OXMapper<T> {
  private static final Logger log = LoggerFactory.getLogger(OXMapper.class);
  private String filePath;
  private Marshaller marshaller;
  private Unmarshaller unmarshaller;

  public String getFilePath() {
    return filePath;
  }

  public void setFilePath(String filePath) {
    this.filePath = filePath;
  }

  public Marshaller getMarshaller() {
    return marshaller;
  }

  public void setMarshaller(Marshaller marshaller) {
    this.marshaller = marshaller;
  }

  public Unmarshaller getUnmarshaller() {
    return unmarshaller;
  }

  public void setUnmarshaller(Unmarshaller unmarshaller) {
    this.unmarshaller = unmarshaller;
  }

  private File getFile(String path) throws IOException {
    return new ClassPathResource(path).getFile();
  }

  public T xmlToObject() throws IOException {
    try(FileInputStream is = new FileInputStream(getFile(filePath))) {
      return (T)this.unmarshaller.unmarshal(new StreamSource(is));
    }
  }
}
