package com.epam.jmp.service.impl;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.List;

import org.springframework.lang.NonNullApi;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;

import com.epam.jmp.model.Event;
import com.epam.jmp.model.Ticket;
import com.epam.jmp.model.Ticket.Category;
import com.epam.jmp.model.User;
import com.epam.jmp.model.exception.EntityNotFoundException;
import com.epam.jmp.model.impl.TicketContainer;
import com.epam.jmp.model.impl.TicketImpl;
import com.epam.jmp.repository.EventRepository;
import com.epam.jmp.repository.TicketRepository;
import com.epam.jmp.repository.UserRepository;
import com.epam.jmp.service.PDFGenerator;
import com.epam.jmp.service.TicketService;

public class TicketServiceImpl implements TicketService {
	private TicketRepository ticketRepository;
	private EventRepository eventRepository;
	private UserRepository userRepository;
	private PDFGenerator<Ticket> ticketPDFGenerator;
	private OXMapper<TicketContainer> oxMapper;
	private TransactionTemplate transactionTemplate;
	
	public TicketServiceImpl(TicketRepository ticketRepository, EventRepository eventRepository, UserRepository userRepository, PDFGenerator<Ticket> ticketPDFGenerator, OXMapper<TicketContainer> unmarshaller, TransactionTemplate transactionTemplate) {
		this.ticketRepository = ticketRepository;
		this.eventRepository = eventRepository;
		this.userRepository = userRepository;
		this.ticketPDFGenerator = ticketPDFGenerator;
		this.oxMapper = unmarshaller;
		this.transactionTemplate = transactionTemplate;
	}

	@Override
	public List<Ticket> getBookedTicket(User user, int pageSize, int pageNumber) {
		return ticketRepository.getBookedTickets(user, pageSize, pageNumber);
	}

	@Override
	public List<Ticket> getBookedTicket(Event event, int pageSize, int pageNumber) {
		return ticketRepository.getBookedTickets(event, pageSize, pageNumber);
	}

	@Override
	public Ticket bookTicket(long userId, long eventId, int place, Category category) {
		boolean isUserPresent = userRepository.isPresent(userId);
		
		if(!isUserPresent) {
			throw new EntityNotFoundException("User with ID: " + userId + " not found");
		}
		
		boolean isEventPresent = eventRepository.isPresent(eventId);
		
		if(!isEventPresent) {
			throw new EntityNotFoundException("Event with ID: " + eventId + " not found");
		}
		return ticketRepository.bookTicket(userId, eventId, place, category);
	}

	@Override
	public boolean cancelTicket(long ticketId) {
		return ticketRepository.cancelTicket(ticketId);
	}

	@Override
	public ByteArrayInputStream generatePDFReport(List<Ticket> tickets) {
		return ticketPDFGenerator.generate(tickets);
	}

	@Override
	public void preloadTickets() {
		transactionTemplate.execute(new TransactionCallbackWithoutResult() {
			@Override
			protected void doInTransactionWithoutResult(TransactionStatus transactionStatus) {
				try {
					TicketContainer ticketContainer = oxMapper.xmlToObject();
					for(Ticket ticket: ticketContainer.getTickets()) {
						bookTicket(ticket.getUserId(), ticket.getEventId(), ticket.getPlace(), ticket.getCategory());
					}
				} catch (IOException e) {
					transactionStatus.setRollbackOnly();
				}
			}
		});
	}
}
