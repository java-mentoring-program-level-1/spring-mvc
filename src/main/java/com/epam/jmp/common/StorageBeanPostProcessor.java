package com.epam.jmp.common;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.core.io.ClassPathResource;

import com.epam.jmp.model.Event;
import com.epam.jmp.model.Ticket;
import com.epam.jmp.model.Ticket.Category;
import com.epam.jmp.model.User;
import com.epam.jmp.model.impl.EventImpl;
import com.epam.jmp.model.impl.TicketImpl;
import com.epam.jmp.model.impl.UserImpl;
import com.epam.jmp.storage.BookingStorage;
public class StorageBeanPostProcessor implements BeanPostProcessor {
	private static Logger log = LoggerFactory.getLogger(StorageBeanPostProcessor.class);
	private String userPath;
	private String eventPath;
	private String ticketPath;
	private String datePattern;
	private final static String splitter = ",";

	public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
		return BeanPostProcessor.super.postProcessBeforeInitialization(bean, beanName);
	}

	public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
		
		if(bean instanceof BookingStorage) {
			BookingStorage storage = ((BookingStorage)bean);
			storage.addAll(getUsers(userPath));
			storage.addAll(getEvents(eventPath));
			storage.addAll(getTickets(ticketPath));
		}
		return BeanPostProcessor.super.postProcessAfterInitialization(bean, beanName);
	}
	
	private Map<String, Object> getUsers(String userPath) {
	
		if(!Objects.isNull(userPath) && userPath.length() > 0) {
			Map<String, Object> users = new HashMap<>();
			try(BufferedReader br = new BufferedReader(new FileReader(getFile(userPath)))) {
				br.readLine(); // skip the first line
				for(String line = br.readLine(); line != null; line = br.readLine()) {
					String[] tokens = line.split(splitter);
					User user = new UserImpl();
					user.setId(Long.valueOf(tokens[0]));
					user.setName(tokens[1]);
					user.setEmail(tokens[2]);
					users.put("user:" + user.getId(), user);
					log.info("Added a new user. ID: {}, name: {}, email: {}", user.getId(), user.getName(), user.getEmail());
				}
				return users;
			} catch(IOException ex) {
				log.error("Failed when reading a user from file. Cause: {}", ex);
			}
		}
		
		return Collections.emptyMap();
	}
	
	private Map<String, Object> getEvents(String eventPath) {
		
		if(!Objects.isNull(eventPath) && eventPath.length() > 0) {
			Map<String, Object> events = new HashMap<>();
			SimpleDateFormat formatter = new SimpleDateFormat(datePattern);
			
				try(BufferedReader br = new BufferedReader(new FileReader(getFile(eventPath)))) {
					br.readLine(); // skip the first line
					for(String line = br.readLine(); line != null; line = br.readLine()) {
						String[] tokens = line.split(splitter);
						Event event = new EventImpl();
						event.setId(Long.valueOf(tokens[0]));
						event.setDate(formatter.parse(tokens[1]));
						event.setTitle(tokens[2]);
						events.put("event:" + event.getId(), event);
						log.info("Added a new event. ID: {}, date: {}, title: {}", event.getId(), event.getDate(), event.getTitle());
					}
					return events;
				} catch(IOException | ParseException ex) {
					log.error("Failed when reading a event from file. Cause: {}", ex);
				}
		}
		return Collections.emptyMap();
	}

	private Map<String, Object> getTickets(String ticketPath) {
		
		if(!Objects.isNull(ticketPath) && ticketPath.length() > 0) {
			Map<String, Object> tickets = new HashMap<String, Object>();
			try(BufferedReader br = new BufferedReader(new FileReader(getFile(ticketPath)))) {
				br.readLine(); // skip the first line
				for(String line = br.readLine(); line != null; line = br.readLine()) {
					String[] tokens = line.split(splitter);
					Ticket ticket = new TicketImpl();
					ticket.setId(Long.valueOf(tokens[0]));
					ticket.setUserId(Long.valueOf(tokens[1]));
					ticket.setEventId(Long.valueOf(tokens[2]));
					ticket.setPlace(Integer.valueOf(tokens[3]));
					ticket.setCategory(Category.valueOf(tokens[4]));
					tickets.put("ticket:" + ticket.getId(), ticket);
					log.info("Added a new ticket. ID: {}, userId: {}, eventId: {}, place: {}, category: {}", ticket.getId(), ticket.getUserId(), ticket.getEventId(), ticket.getPlace(), ticket.getCategory());
				}
				return tickets;
			} catch(IOException ex) {
				log.error("Failed when reading a ticket from file. Cause: {}", ex);
			}
		}
		return Collections.emptyMap();
	}
	
	private File getFile(String path) throws IOException {
		return new ClassPathResource(path).getFile();
	}
	
	public void setUserPath(String userPath) {
		this.userPath = userPath;
	}

	public void setEventPath(String eventPath) {
		this.eventPath = eventPath;
	}

	public void setTicketPath(String ticketPath) {
		this.ticketPath = ticketPath;
	}
	
	public void setDatePattern(String datePattern) {
		this.datePattern = datePattern;
	}
	
	
		
}
