package com.epam.jmp.common;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.epam.jmp.facade.BookingFacade;
import com.epam.jmp.model.Event;
import com.epam.jmp.model.Ticket;
import com.epam.jmp.model.Ticket.Category;
import com.epam.jmp.model.User;
import com.epam.jmp.model.exception.EntityNotFoundException;
import com.epam.jmp.model.impl.EventImpl;
import com.epam.jmp.model.impl.UserImpl;

public class SpringRunner {
	private static Logger log = LoggerFactory.getLogger(SpringRunner.class);
	public static void main(String[] args) {
		
		ApplicationContext ctx = new ClassPathXmlApplicationContext("config/services-context.xml");
		BookingFacade facade = (BookingFacade) ctx.getBean("bookingFacade");
		
		/** Create an user */
		User john = new UserImpl();
		john.setName("John Depp");
		john.setEmail("john_depp@email.com");
		john = facade.createUser(john);
		log.info("Created an user: ID: {}, name: {}, email: {}", john.getId(), john.getName(), john.getEmail());
		
		/** Create an event*/
		Event multiverse = new EventImpl();
		multiverse.setDate(new Date());
		multiverse.setTitle("Epam: Multiverse party");
		multiverse = facade.createEvent(multiverse);
		log.info("Created an event: ID: {}, date: {}, title: {}", multiverse.getId(), multiverse.getDate(), multiverse.getTitle());
		
		/** Book a ticket */
		Ticket ticket = null;
		try {
			ticket = facade.bookTicket(john.getId(), multiverse.getId(), 16, Category.BAR);	
			log.info("Booked a ticket: ID: {}, userId: {}, eventId: {}, place: {}, category: {}", ticket.getId(), ticket.getUserId(), ticket.getEventId(), ticket.getPlace(), ticket.getCategory());
		} catch(EntityNotFoundException ex) {
			log.error("Failed a ticket. Cause: {}", ex);
		}
		
		/** Cancel the ticket */
		if(ticket != null) {
			facade.cancelTicket(ticket.getId());	
			log.info("Cancelled the ticket.");
		}
	}
}
