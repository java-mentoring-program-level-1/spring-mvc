package com.epam.jmp.repository.impl;

import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.epam.jmp.model.Event;
import com.epam.jmp.model.Ticket;
import com.epam.jmp.model.Ticket.Category;
import com.epam.jmp.model.User;
import com.epam.jmp.model.exception.EntityNotFoundException;
import com.epam.jmp.model.impl.TicketImpl;
import com.epam.jmp.repository.TicketRepository;
import com.epam.jmp.storage.BookingStorage;

public class TicketRepositoryImpl implements TicketRepository {
	Logger log = LoggerFactory.getLogger(TicketRepositoryImpl.class);
	private BookingStorage storage;

	public TicketRepositoryImpl(BookingStorage storage) {
		this.storage = storage;
	}

	@Override
	public Ticket bookTicket(long userId, long eventId, int place, Category category) {
		if(place <= 0) {
			throw new IllegalArgumentException("Ticket contains invalid place number");
		}
		
		if(isPlaceBooked(place, category)) {
			throw new IllegalStateException("Place with number: " + place + " was already booked");
		}
		
		Optional<Entry<String, Object>> ticketOptional = storage.getStorage().entrySet().stream()
				.filter(p -> {
					if(p.getKey().contains("ticket"))
						return true;
					return false;})
				.max((entry1, entry2) -> Long.compare(((Ticket)entry1.getValue()).getId(), ((Ticket)entry2.getValue()).getId()));
		
		Ticket ticket = new TicketImpl();
		ticket.setCategory(category);
		ticket.setEventId(eventId);
		ticket.setUserId(userId);
		ticket.setPlace(place);
		
		if(ticketOptional.isPresent()) {
			ticket.setId(((Ticket)ticketOptional.get().getValue()).getId() + 1);
		} else {
			ticket.setId(1L);
		}
		
		storage.put("ticket:" + ticket.getId() , ticket);
		log.info("Inserted a ticket: ID: {}, userId: {}, eventId: {}, place: {}, category: {}", ticket.getId(), ticket.getUserId(), ticket.getEventId(), ticket.getPlace(), ticket.getCategory());
		return ticket;
	}
	
	private boolean isPlaceBooked(int place, Category category) {
		return this.storage.getStorage().entrySet().stream()
				.filter(entry -> entry.getKey().contains("ticket"))
				.filter(t -> ((Ticket) t.getValue()).getCategory() == category)
				.anyMatch(c -> ((Ticket) c.getValue()).getPlace() == place);
	}
	
	@Override
	public List<Ticket> getBookedTickets(User user, int pageSize, int pageNumber) {
		int skip = (pageNumber - 1) * pageSize;
		return this.storage.getStorage().entrySet().stream()
			.filter(entry -> entry.getKey().contains("ticket"))
			.filter(ticket -> ((Ticket) ticket.getValue()).getUserId() == user.getId())
			.sorted((e1, e2) -> {
				Date date1 = getEventDateByEventId(((Ticket)e1.getValue()).getEventId());
				Date date2 = getEventDateByEventId(((Ticket)e2.getValue()).getEventId());
				return date2.compareTo(date1);
			})
			.skip(skip)
			.limit(pageSize)
			.flatMap(e -> Stream.of((Ticket)e.getValue())).collect(Collectors.toList());
	}
	
	private Date getEventDateByEventId(long eventId) {
		return ((Event)this.storage.getStorage().get("event:" + eventId)).getDate();
	}

	@Override
	public List<Ticket> getBookedTickets(Event event, int pageSize, int pageNumber) {
		int skip = (pageNumber - 1) * pageSize;
		return this.storage.getStorage().entrySet().stream()
			.filter(entry -> entry.getKey().contains("ticket"))
			.filter(ticket -> ((Ticket) ticket.getValue()).getEventId() == event.getId())
			.sorted((e1, e2) -> {
				String email1 = getEmailByUserId(((Ticket)e1.getValue()).getUserId());
				String email2 = getEmailByUserId(((Ticket)e2.getValue()).getUserId());
				return email1.compareTo(email2);
			})
			.skip(skip)
			.limit(pageSize)
			.flatMap(e -> Stream.of((Ticket)e.getValue())).collect(Collectors.toList());
	}

	private String getEmailByUserId(long userId) {
		return ((User)this.storage.getStorage().get("user:" + userId)).getEmail();
	}

	@Override
	public boolean cancelTicket(long ticketId) {
		Ticket ticket =(Ticket) this.storage.remove("ticket:" + ticketId);
		if(ticket != null) {
			log.info("Cancelled a ticket: ID: {}, userId: {}, eventId: {}, place: {}, category: {}", ticket.getId(), ticket.getUserId(), ticket.getEventId(), ticket.getPlace(), ticket.getCategory());
			return true;
		}
		return this.storage.remove("ticket:" + ticketId) != null;
	}

	

	
}
