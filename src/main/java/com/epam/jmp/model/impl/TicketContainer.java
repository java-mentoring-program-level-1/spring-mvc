package com.epam.jmp.model.impl;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "tickets")
public class TicketContainer {
  private List<TicketImpl> tickets;

  @XmlElement(name = "ticket")
  public List<TicketImpl> getTickets() {
    return tickets;
  }

  public void setTickets(List<TicketImpl> tickets) {
    this.tickets = tickets;
  }
}
