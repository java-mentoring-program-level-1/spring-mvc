package com.epam.jmp.model.impl;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import com.epam.jmp.model.Ticket;

@XmlRootElement
@XmlType(propOrder = {"userId", "eventId", "category", "place"})
public class TicketImpl implements Ticket {
	private long id;
	private long eventId;
	private long userId;
	private Category category = Category.STANDARD; // default value
	private int place;

	@XmlTransient
	@Override
	public long getId() {
		return this.id;
	}

	@Override
	public void setId(long id) {
		this.id = id;
	}

	@XmlAttribute(name = "eventId")
	@Override
	public long getEventId() {
		return this.eventId;
	}

	@Override
	public void setEventId(long eventId) {
		this.eventId = eventId;
	}

	@XmlAttribute(name = "userId")
	@Override
	public long getUserId() {
		return this.userId;
	}

	@Override
	public void setUserId(long userId) {
		this.userId = userId;
	}

	@XmlAttribute(name = "category")
	@Override
	public Category getCategory() {
		return this.category;
	}

	@Override
	public void setCategory(Category category) {
		this.category = category;
	}

	@XmlAttribute(name = "place")
	@Override
	public int getPlace() {
		return this.place;
	}

	@Override
	public void setPlace(int place) {
		this.place = place;
	}

}
