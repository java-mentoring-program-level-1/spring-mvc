package com.epam.jmp.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.epam.jmp.facade.BookingFacade;
import com.epam.jmp.model.Event;
import com.epam.jmp.model.impl.EventImpl;

@Controller
@PropertySource("classpath:application.properties")
public class EventController {
  private static Logger log = LoggerFactory.getLogger(EventController.class);
  private static final String VIEW = "event.html";
  private static final String DEFAULT_PAGE = "1";
  private static final String DEFAULT_SIZE = "5";

  @Autowired
  private BookingFacade facade;
  @Autowired
  private SimpleDateFormat formatter;

  @GetMapping(value = "/events", params = {"title", "page", "size"})
  public ModelAndView getEventsByTitle(@RequestParam("title") String title,
    @RequestParam(value = "page", required = false, defaultValue = DEFAULT_PAGE) int page,
    @RequestParam(value = "size", required = false, defaultValue = DEFAULT_SIZE) int size,
    ModelAndView modelAndView) {
    log.info("get events by title");
    modelAndView.setViewName(VIEW);

    List<Event> events = facade.getEventsByTitle(title, size, page);
    modelAndView.addObject("events", events);

    return modelAndView;
  }

  @GetMapping(value = "/events", params = {"date", "page", "size"})
  public ModelAndView getEventsForDate(@RequestParam("date") @DateTimeFormat(pattern = "yyyy-MM-dd") Date date,
    @RequestParam(value = "page", required = false, defaultValue = DEFAULT_PAGE) int page,
    @RequestParam(value = "size", required = false, defaultValue = DEFAULT_SIZE) int size,
    ModelAndView modelAndView) throws ParseException {
    log.info("get events for date");
    modelAndView.setViewName(VIEW);
    formatter.applyPattern("dd-MM-yyyy");
    List<Event> events = facade.getEventsForDay(date, size, page);
    modelAndView.addObject("events", events);
    return modelAndView;
  }

  @GetMapping(value = "/events", params = "id")
  public ModelAndView getEventById(@RequestParam("id") long id,
    ModelAndView modelAndView) {
    log.info("get event by id");
    modelAndView.setViewName(VIEW);
    Event event = facade.getEventById(id);
    modelAndView.addObject("events", event);
    return modelAndView;
  }

  @GetMapping(value = "/events", params = {"_method", "id"})
  public ModelAndView deleteEvent(@RequestParam("_method") String method,
    @RequestParam("id") long id, ModelAndView modelAndView) {
    log.info("deleting event");
    modelAndView.setViewName(VIEW);

    if("delete".equals(method)) {
      boolean result = facade.deleteEvent(id);
      String message = "Event with id = " + id + " is ";
      message += result ? "deleted successfully" : "not deleted";

      modelAndView.addObject("message", message);
    }
    return modelAndView;
  }

  @PostMapping(value = "/events", params = {"title", "date"})
  public ModelAndView insertEvent(@RequestParam("title") String title,
    @RequestParam("date") @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm") Date date, ModelAndView modelAndView) {
    log.info("inserting event");
    modelAndView.setViewName(VIEW);

    Event event = new EventImpl();
    event.setTitle(title);
    event.setDate(date);

    Event result = facade.createEvent(event);
    modelAndView.addObject("events", result);

    return modelAndView;
  }

  @GetMapping(value = "/events", params = {"_method", "id", "title", "date"})
  public ModelAndView updateUser(@RequestParam("_method") String method,
    @RequestParam("id") long id,
    @RequestParam(value = "title", required = false) String title,
    @RequestParam(value = "date", required = false) @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm") Date date,
    ModelAndView modelAndView) {
    log.info("updating event");
    modelAndView.setViewName(VIEW);

    if("update".equals(method)) {
      Event event = new EventImpl();
      event.setId(id);
      event.setTitle(title);
      event.setDate(date);
      Event result = facade.updateEvent(event);
      modelAndView.addObject("events", result);
    }

    return modelAndView;
  }
}
