package com.epam.jmp.controller;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;

import com.epam.jmp.model.exception.EntityNotFoundException;

@ControllerAdvice
public class ControllerExceptionHandler {
  private static final Logger log = LoggerFactory.getLogger(ControllerExceptionHandler.class);
  private static final String VIEW = "error.html";

  @ExceptionHandler(value = EntityNotFoundException.class)
  @ResponseStatus(HttpStatus.NOT_FOUND)
  public ModelAndView handleEntityException(HttpServletRequest request, EntityNotFoundException ex) {
    log.info("handling entity exception");
    ModelAndView modelAndView = new ModelAndView();
    modelAndView.setViewName(VIEW);

    modelAndView.addObject("message", ex.getMessage());
    modelAndView.addObject("url", request.getRequestURL());

    return modelAndView;
  }

  @ExceptionHandler(value = IllegalArgumentException.class)
  @ResponseStatus(HttpStatus.BAD_REQUEST)
  public ModelAndView handleEntityException(HttpServletRequest request, IllegalArgumentException ex) {
    log.info("handling illegal argument exception");
    ModelAndView modelAndView = new ModelAndView();
    modelAndView.setViewName(VIEW);

    modelAndView.addObject("message", ex.getMessage());
    modelAndView.addObject("url", request.getRequestURL());

    return modelAndView;
  }
}
