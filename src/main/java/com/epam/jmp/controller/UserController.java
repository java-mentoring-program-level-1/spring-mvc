package com.epam.jmp.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.epam.jmp.facade.BookingFacade;
import com.epam.jmp.model.User;
import com.epam.jmp.model.impl.UserImpl;
import com.epam.jmp.service.UserService;

@Controller
public class UserController {
	@Autowired
	private BookingFacade facade;
	private static final String VIEW = "user.html";
	private static Logger log = LoggerFactory.getLogger(UserController.class);
	private static final String DEFAULT_PAGE = "1";
	private static final String DEFAULT_SIZE = "5";

	@GetMapping(value = "/users", params = "email")
	public ModelAndView getUserByEmail(@RequestParam("email") String email, ModelAndView modelAndView) {
		log.info("get user by email");
		modelAndView.setViewName(VIEW);

		User user = facade.getUserByEmail(email);
		modelAndView.addObject("users", user);

		return modelAndView;
	}

	@GetMapping(value = "/users", params = "id")
	public ModelAndView getUserById(@RequestParam("id") long id, ModelAndView modelAndView) {
		log.info("get user by id");
		modelAndView.setViewName(VIEW);

		User user = facade.getUserById(id);
		modelAndView.addObject("users", user);

		return modelAndView;
	}

	@GetMapping(value = "/users", params={"name", "page", "size"})
	public ModelAndView getUsersByName(@RequestParam("name") String name,
		@RequestParam(value = "page", required = false, defaultValue = DEFAULT_PAGE) int page,
		@RequestParam(value = "size", required = false, defaultValue = DEFAULT_SIZE) int size,
		ModelAndView modelAndView) {
		log.info("get users by name");
		modelAndView.setViewName(VIEW);

		List<User> users = facade.getUsersByName(name, size, page);
		modelAndView.addObject("users", users);

		return modelAndView;
	}

	@PostMapping(value = "users", params = {"name", "email"})
	public ModelAndView createUser(@RequestParam("name") String name,
		@RequestParam("email") String email, ModelAndView modelAndView) {
		log.info("create user");
		modelAndView.setViewName(VIEW);

		UserImpl user = new UserImpl();
		user.setEmail(email);
		user.setName(name);
		User result = facade.createUser(user);
		modelAndView.addObject("users", result);

		return modelAndView;
	}

	@GetMapping(value = "/users", params = {"_method", "id"})
	public ModelAndView deleteUser(@RequestParam("_method") String method,
		@RequestParam("id") long id, ModelAndView modelAndView) {
		log.info("deleting user");
		modelAndView.setViewName(VIEW);

		if("delete".equals(method)) {
			boolean result = facade.deleteUser(id);
			String message = "User with id = " + id + " is ";
			message += result ? "deleted successfully" : "not deleted";

			modelAndView.addObject("message", message);
		}
		return modelAndView;
	}

	@GetMapping(value = "/users", params = {"_method", "id", "name", "email"})
	public ModelAndView updateUser(@RequestParam("_method") String method,
		@RequestParam("id") long id,
		@RequestParam(value = "name", required = false) String name,
		@RequestParam(value = "email", required = false) String email,
		ModelAndView modelAndView) {
		log.info("updating user");
		modelAndView.setViewName(VIEW);

		if("update".equals(method)) {
			User user = new UserImpl();
			user.setId(id);
			user.setEmail(email);
			user.setName(name);
			User result = facade.updateUser(user);
			modelAndView.addObject("users", result);
		}

		return modelAndView;
	}
}
