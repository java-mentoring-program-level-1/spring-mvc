package com.epam.jmp.controller;

import java.io.ByteArrayInputStream;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.epam.jmp.facade.BookingFacade;
import com.epam.jmp.model.Event;
import com.epam.jmp.model.Ticket;
import com.epam.jmp.model.User;

@Controller
public class TicketController {
	Logger log = LoggerFactory.getLogger(TicketController.class);
	private static final String VIEW = "ticket.html";
	private static final String DEFAULT_PAGE = "1";
	private static final String DEFAULT_SIZE = "5";

	@Autowired
	private BookingFacade facade;
	
	@GetMapping(value = "/tickets", params = {"userId", "page", "size"})
	public ModelAndView getTicketsByUser(
		@RequestParam(value = "userId", required = false ) long userId,
		@RequestParam(value = "page", required = false, defaultValue = DEFAULT_PAGE) int page,
		@RequestParam(value = "size", required = false, defaultValue = DEFAULT_SIZE) int size,
		ModelAndView modelAndView) {
		log.info("get booked tickets by user");
		modelAndView.setViewName(VIEW);

		User user = facade.getUserById(userId);

		List<Ticket> bookedTickets = facade.getBookedTickets(user, size, page);
		modelAndView.addObject("tickets", bookedTickets);

		return modelAndView;
	}

	@GetMapping(value = "/tickets", params = {"userId", "page", "size"}, produces = MediaType.APPLICATION_PDF_VALUE)
	public ResponseEntity<InputStreamResource> getTicketsReport(
		@RequestParam(value = "userId", required = false ) long userId,
		@RequestParam(value = "page", required = false, defaultValue = DEFAULT_PAGE) int page,
		@RequestParam(value = "size", required = false, defaultValue = DEFAULT_SIZE) int size) {
		log.info("get booked tickets report");

		User user = facade.getUserById(userId);

		List<Ticket> bookedTickets = facade.getBookedTickets(user, size, page);
		ByteArrayInputStream inputStream = facade.ticketReport(bookedTickets);

		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Disposition", "inline; filename=ticket-report.pdf");

		return ResponseEntity.ok().headers(headers).contentType(MediaType.APPLICATION_PDF).body(new InputStreamResource(inputStream));
	}

	@GetMapping(value = "/tickets", params = {"eventId", "page", "size"})
	public ModelAndView getTicketsByEventId(@RequestParam("eventId") long eventId,
		@RequestParam(value = "page", required = false, defaultValue = DEFAULT_PAGE) int page,
		@RequestParam(value = "size", required = false, defaultValue = DEFAULT_SIZE) int size,
		ModelAndView modelAndView) {
		log.info("get booked tickets by event");
		modelAndView.setViewName(VIEW);

		Event event = facade.getEventById(eventId);

		List<Ticket> bookedTickets = facade.getBookedTickets(event, size, page);
		modelAndView.addObject("tickets", bookedTickets);
		log.info("tickets by event : " + bookedTickets);
		return modelAndView;
	}

	@PostMapping(value = "/tickets", params = {"userId", "eventId", "place", "category"})
	public ModelAndView bookTicket(@RequestParam("userId") long userId,
		@RequestParam("eventId") long eventId, @RequestParam("place") int place,
		@RequestParam(value = "category" , required = false) Ticket.Category category,
		ModelAndView modelAndView) {
		log.info("book a ticket");
		modelAndView.setViewName(VIEW);

		Ticket ticket = facade.bookTicket(userId, eventId, place, category);
		String message = "Ticket booked successfully";
		modelAndView.addObject("message", message);
		modelAndView.addObject("tickets", List.of(ticket));
		return modelAndView;
	}

	@GetMapping(value = "/tickets", params = {"_method", "id"})
	public ModelAndView cancelTicket(@RequestParam("_method")String method, @RequestParam("id") long id, ModelAndView modelAndView) {
		log.info("cancelling ticket");
		modelAndView.setViewName(VIEW);

		if("delete".equals(method)) {
			boolean result = facade.cancelTicket(id);
			String message = "Ticket with id = " + id + " is ";
			message += result ? "cancelled successfully" : "not cancelled";

			modelAndView.addObject("message", message);
		}
		return modelAndView;
	}

	@GetMapping("/tickets/load")
	public ModelAndView preloadTickets(ModelAndView modelAndView) {
		log.info("preloading tickets");
		modelAndView.setViewName(VIEW);

		facade.preloadTickets();
		modelAndView.addObject("message", "preloaded successfully");

		return modelAndView;
	}
}
