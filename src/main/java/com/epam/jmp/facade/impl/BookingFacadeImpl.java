package com.epam.jmp.facade.impl;

import java.io.ByteArrayInputStream;
import java.util.Date;
import java.util.List;

import com.epam.jmp.facade.BookingFacade;
import com.epam.jmp.model.Event;
import com.epam.jmp.model.Ticket;
import com.epam.jmp.model.Ticket.Category;
import com.epam.jmp.model.exception.EntityNotFoundException;
import com.epam.jmp.model.User;
import com.epam.jmp.service.EventService;
import com.epam.jmp.service.TicketService;
import com.epam.jmp.service.UserService;

public class BookingFacadeImpl implements BookingFacade {

	private UserService userService;
	private EventService eventService;
	private TicketService ticketService;
	
	public BookingFacadeImpl(UserService userService, EventService eventService, TicketService ticketService) {
		this.userService = userService;
		this.eventService = eventService;
		this.ticketService = ticketService;
	}
	
	@Override
	public Event getEventById(long eventId) {
		return eventService.getEventById(eventId).orElseThrow(() -> {
			throw new EntityNotFoundException("Event with ID: " + eventId + " not found");
		});
	}

	@Override
	public List<Event> getEventsByTitle(String title, int pageSize, int pageNum) {
		return eventService.getEventsByTitle(title, pageSize, pageNum);
	}

	@Override
	public List<Event> getEventsForDay(Date day, int pageSize, int pageNum) {
		return eventService.getEventsForDay(day, pageSize, pageNum);
	}

	@Override
	public Event createEvent(Event event) {
		return eventService.createEvent(event);
	}

	@Override
	public Event updateEvent(Event event) {
		return eventService.updateEvent(event);
	}

	@Override
	public boolean deleteEvent(long eventId) {
		return eventService.deleteEventById(eventId);
	}

	@Override
	public User getUserById(long userId) {
		return userService.getUserById(userId).orElseThrow(() -> {
			throw new EntityNotFoundException("User with ID: " + userId + " not found");
		});
	}

	@Override
	public User getUserByEmail(String email) {
		return userService.getUserByEmail(email).orElseThrow(() -> {
			throw new EntityNotFoundException("User with Email: " + email + " not found");
		});
	}

	@Override
	public List<User> getUsersByName(String name, int pageSize, int pageNum) {
		return userService.getUsersByName(name, pageSize, pageNum);
	}

	@Override
	public User createUser(User user) {
		return userService.createUser(user);
	}

	@Override
	public User updateUser(User user) {
		return userService.updateUser(user);
	}

	@Override
	public boolean deleteUser(long userId) {
		return userService.deleteUserById(userId);
	}

	@Override
	public Ticket bookTicket(long userId, long eventId, int place, Category category) {
		return ticketService.bookTicket(userId, eventId, place, category);
	}

	@Override
	public List<Ticket> getBookedTickets(User user, int pageSize, int pageNum) {
		return ticketService.getBookedTicket(user, pageSize, pageNum);
	}

	@Override
	public List<Ticket> getBookedTickets(Event event, int pageSize, int pageNum) {
		return ticketService.getBookedTicket(event, pageSize, pageNum);
	}

	@Override
	public boolean cancelTicket(long ticketId) {
		return ticketService.cancelTicket(ticketId);
	}

	@Override
	public ByteArrayInputStream ticketReport(List<Ticket> bookedTickets) {
		return ticketService.generatePDFReport(bookedTickets);
	}

	@Override
	public void preloadTickets() {
		 ticketService.preloadTickets();
	}

}
